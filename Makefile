#!/usr/bin/env make


up:
	docker-compose up -d

down:
	docker-compose down

bash:
	@docker-compose run --rm gulp bash	